'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var changed = require('gulp-changed');
var runSequence = require('run-sequence');
var neat = require('node-neat');
var babel = require('gulp-babel');

var node_modules = './node_modules/';
var jqueryPath = node_modules + 'jquery/dist/*.min.js';

//Build out the paths to the sass libraries
var sassIncludePaths = [
    __dirname+'/node_modules/normalize-libsass',
];
sassIncludePaths = sassIncludePaths.concat(require('node-neat').includePaths);

gulp.task('sass', function(){
	gulp.src('./scss/app.scss')
		.pipe(sass({includePaths: sassIncludePaths}))
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('../dist/css/'));
});

gulp.task('copy', function(){
	gulp.src(jqueryPath)
		.pipe(changed('../dist/js/'))
		.pipe(gulp.dest('../dist/js/'));

	gulp.src('./example.html')
		.pipe(changed('../dist/'))
		.pipe(gulp.dest('../dist/'));

	gulp.src('./js/agegate.js')
		.pipe(changed('../dist/js/'))
		.pipe(babel({"presets": ["env"]}))
		.pipe(gulp.dest('../dist/js/'));
});

gulp.task('watch', function(){
	gulp.watch('./scss/**/*.scss', ['sass']);
	gulp.watch(jqueryPath, ['copy']);
	gulp.watch('./js/agegate.js', ['copy']);
	gulp.watch('./example.html', ['copy']);

});

gulp.task('build', function(callback){
	runSequence(['sass', 'copy'],
		 callback
	);
});

gulp.task('default', function (callback) {
  runSequence(['sass','copy', 'watch'],
    callback
  );
});