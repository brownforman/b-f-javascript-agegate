# BF Javascript Agegate #

This agegate works by showing and hiding div elements inside of your page layout.  This requires all of the agegated content to be inside of ``<div id="body-outer"></div>``, the age gate HTML goes inside of ``<div id="agegate-outer"></div>``.  Check out ``dist/example.html`` or ``dist/example.php`` for more details.

## Basic Usage ##
1. Include the age-gate HTML into your layout of your project (Look at dist/example.html)
2. Include dist/js/agegate.js and either add your own styles or include dist/css/app.css

## Default Country Option ##
Set the data-default-iso attribute in the country select element in order to default the country. Use ISO-2 value. Will default to US if not set.

If using PHP on a Cloudflare enabled site, use the age gate from example.php to use the HTTP_CF_IPCOUNTRY Cloudflare header to get user country.

## Enable Auto Tabbing
Add data-auto-tab="true" to the agegate form element to enable auto tabbing